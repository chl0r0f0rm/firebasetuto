//
//  ProfileTableViewCell.m
//  chatapp
//
//  Created by cmouline on 22/10/2017.
//  Copyright © 2017 Chloe MOULINET. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
